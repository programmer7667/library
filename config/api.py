from rest_framework import routers
from libraryApi.views import BookViewSet


router = routers.DefaultRouter()


router.register(r'book', BookViewSet)

