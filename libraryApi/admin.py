from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UserCreationForm
from .models import User


class UserAdmin(UserAdmin):
    add_form = UserCreationForm
    model = User
    fieldsets = ()
    add_fieldsets = (
        (None, {
            'fields': ('username', 'role', 'password1', 'password2')}),
            ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )


admin.site.register(User, UserAdmin)
