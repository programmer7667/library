# Generated by Django 4.1.7 on 2023-02-22 05:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libraryApi', '0002_alter_user_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.SmallIntegerField(choices=[(1, 'Author'), (2, 'Editor')], null=True),
        ),
    ]
