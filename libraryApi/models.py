from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class User(AbstractUser):
    ROLE_CHOICES = (
        (1, 'Author'),
        (2, 'Editor'),

    )
    role = models.SmallIntegerField(choices = ROLE_CHOICES, null = True)

class Book(models.Model):
    name = models.CharField(max_length = 100)
    author = models.ForeignKey(User, on_delete = models.CASCADE)
    
    def __str__(self):
        return self.name

    

    
    