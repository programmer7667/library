from rest_framework import permissions


SAFE_METHODS1 = ['GET', 'POST', 'DELETE']
SAFE_METHODS2 = ['GET', 'PUT']

class UserPermission(permissions.IsAuthenticated):

    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.is_active:
            if request.method in SAFE_METHODS1 and request.user.role == 1:
                return True

            elif request.method in SAFE_METHODS2 and request.user.role == 2:
                return True
                
        return False

