from rest_framework import viewsets, generics
from .models import Book, User
from .serializers import BookSerializer
from .permissions import UserPermission


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (UserPermission, )
   

    def perform_create(self, serializer):
        serializer.save(author = self.request.user)
    
